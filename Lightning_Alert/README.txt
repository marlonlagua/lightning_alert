﻿AUTHOR: Marlon Lagua
DESCRIPTION: A Console program that displays an alert to an "Asset" when a "Strike" occurs.  The system will not display an alert if an "Asset" is not found.
The system uses Newtonsoft.Json, which can be downloaded via Nuget, to parse files into valid objects i.e.: Asset, Strike

HOW IT WORKS
The system takes two arguments:
(1) Strike information, this can by a json string or a file path containing the strike information.
(2) Asset information, a file path containing the list of Assets.

**EXAMPLE EXECUTION COMMAND

Lightning_Alert D:\Test\lightning-alert\lightning.json D:\Test\lightning-alert\assets.json

WHERE D:\Lightning\lightning_alert\lightning.json is a file containing the lightning strikes
AND D:\Lightning\lightning_alert\assets.json is a file containing the list of assets


QUESTION(S):

Q: What is the time complexity for determining if a strike has occurred for a particular asset?
A: In my implementation the time complexity of determining if a strike for a particular asset is O(n) since I used a list to find quadkeys that was already alerted.

Q: If we put this code into production, but found it too slow, or it needed to scale to many more users or more frequent strikes, what are the first things you would think of to speed it up?
A: First I would think of caching the list of Assets, even though it is small, converting a json file to an object still cost processing.
	Addtionlly, assets aren't often changed so putting it into a cache would save processing, of course we still need to properly put in an expiration logic to it so when changes to an asset
	occurs, we'll be sure to have an updated asset information.
   Another thing that I would look into is the possiblity of implementing asynchronous processing, this way the system may take multiple calls without waiting for the other process to complete.