﻿namespace Lightning_Alert.Helper
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Newtonsoft.Json;

    public static class JsonHelper
    {
        #region Methods

        /// <summary>
        /// Reads a json file or json string into a T list.
        /// </summary>
        /// <typeparam name="T">the type of the object</typeparam>
        /// <param name="jsonFile">the json file or json string</param>
        /// <returns>a list of T</returns>
        public static List<T> ReadToList<T>(string jsonFile)
            where T : class
        {
            var returnList = new List<T>();
            if (IsValidPath(jsonFile))
            {
                //// read from file
                returnList = DeserializeFromFile<T>(jsonFile);
            }
            else
            {
                //// must be string
                returnList = DeserializeFromString<T>(jsonFile);
            }

            return returnList;
        }

        /// <summary>
        /// Reads a json string into an object
        /// </summary>
        /// <typeparam name="T">the type of the object</typeparam>
        /// <param name="jsonString">the json string</param>
        /// <returns>returns a T from the json string.</returns>
        public static T ReadToObject<T>(string jsonString)
            where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch
            {
                Console.WriteLine("Failed to convert file to object {0}", typeof(T));
                return null;
            }
        }

        /// <summary>
        /// Deserializes a json file into a list of T
        /// </summary>
        /// <typeparam name="T">the type of the object</typeparam>
        /// <param name="filePath">the valid file path</param>
        /// <returns>a list of T</returns>
        private static List<T> DeserializeFromFile<T>(string filePath)
            where T : class
        {
            try
            {
                using (StreamReader file = File.OpenText(filePath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    return (List<T>)serializer.Deserialize(file, typeof(List<T>));
                }
            }
            catch
            {
                Console.WriteLine("Failed to convert file to object {0}", typeof(T));
                return new List<T>();
                
            }
        }

        /// <summary>
        /// Desirializes a json string into a list of T
        /// </summary>
        /// <typeparam name="T">the type of the object</typeparam>
        /// <param name="jsonString">the json string</param>
        /// <returns>returns a list of T</returns>
        private static List<T> DeserializeFromString<T>(string jsonString)
            where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<List<T>>(jsonString);
            }
            catch
            {
                Console.WriteLine("Failed to convert jsonstring to object {0}", typeof(T));
                return new List<T>();
            }
        }

        /// <summary>
        /// Checks if the given string is a valid local path.
        /// </summary>
        /// <param name="filePath">the local path.</param>
        /// <returns>true if the given local path is valid; else false</returns>
        private static bool IsValidPath(string filePath)
        {
            try
            {
                Path.GetFullPath(filePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion Methods
    }
}