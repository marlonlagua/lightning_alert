﻿namespace Lightning_Alert.Helper
{
    using System.Collections.Generic;
    using System.Linq;

    using Entities;

    public static class StrikeHelper
    {
        #region Fields

        /// <summary>Zoom _level</summary>
        private const int zoom_level = 12;

        /// <summary>Alerted</summary>
        private static List<string> alerted = new List<string>();

        /// <summary>Asset dictionary</summary>
        private static Dictionary<string, Asset> assetDictionary = new Dictionary<string, Asset>();

        /// <summary>Flash types</summary>
        private static List<FlashType> flashTypes = new List<FlashType>()
            {
                new FlashType() { Id = 0, Description = "cloud to ground", LightningStrike = true },
                new FlashType() { Id = 1, Description = "cloud to ground", LightningStrike = true },
                new FlashType() { Id = 9, Description = "heartbeat", LightningStrike = false }
            };

        #endregion Fields

        #region Methods

        /// <summary>
        /// Gets the asset for the given strike
        /// </summary>
        /// <param name="strike">the strike object</param>
        /// <returns>returns an asset object for the given strike if found else null</returns>
        public static Asset GetAsset(Strike strike)
        {
            if (strike != null)
            {
                var type = flashTypes.FirstOrDefault(f => f.Id == strike.FlashType);
                if (type!= null && type.LightningStrike)
                {
                    var quadKey = MapHelper.GetQuadKey(strike.Latitude, strike.Longitude, zoom_level);
                    if (assetDictionary.ContainsKey(quadKey) && !alerted.Contains(quadKey))
                    {
                        alerted.Add(quadKey);
                        return assetDictionary[quadKey];
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the assets
        /// </summary>
        /// <param name="assets">the list of assets</param>
        public static void SetAssets(List<Asset> assets)
        {
            assetDictionary = assets.ToDictionary(x => x.QuadKey, v => v);
        }

        #endregion Methods
    }
}