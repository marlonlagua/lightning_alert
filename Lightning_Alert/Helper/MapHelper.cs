﻿namespace Lightning_Alert.Helper
{
    using Microsoft.MapPoint;

    /// <summary>
    /// Class that implements the map helper functions
    /// </summary>
    public static class MapHelper
    {
        #region Methods

        /// <summary>
        /// Gets the quadkey given the longitude, latitude and the zoom level
        /// </summary>
        /// <param name="latitude">the latitude</param>
        /// <param name="longitude">the longitude</param>
        /// <param name="levelOfDetail">the zoom level</param>
        /// <returns>returns a string quadkey</returns>
        public static string GetQuadKey(double latitude, double longitude, int levelOfDetail)
        {
            int pixelX, pixelY, tileX, tileY;
            TileSystem.LatLongToPixelXY(latitude, longitude, levelOfDetail, out pixelX, out pixelY);
            TileSystem.PixelXYToTileXY(pixelX, pixelY, out tileX, out tileY);
            return TileSystem.TileXYToQuadKey(tileX, tileY, levelOfDetail);
        }

        #endregion Methods
    }
}