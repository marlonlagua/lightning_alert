﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Merlot Aero Limited">
//     Copyright © Merlot Aero Limited 2008-2021. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Lightning_Alert
{
    using Entities;
    using Lightning_Alert.Helper;
    using System;
    using System.IO;

    class Program
    {
        #region Methods

        static void Main(string[] args)
        {
            //// We need two arguments to execute.
            if (args.Length == 2)
            {
                //// we've recieved a valid input, let's try to parse it.
                var assets = JsonHelper.ReadToList<Asset>(args[1]);
                StrikeHelper.SetAssets(assets);
                Console.WriteLine("Assets count: {0}", assets.Count);
                foreach (var line in File.ReadLines(args[0]))
                {
                    try
                    {
                        //// Read objects one by one and get asset if possible.
                        var strike = JsonHelper.ReadToObject<Strike>(line);
                        var asset = StrikeHelper.GetAsset(strike);
                        if (asset != null)
                        {
                            Console.WriteLine("lightning alert for {0} : {1}", asset.AssetOwner, asset.AssetName);
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Strike is in correct format");
                    }
                }
            }

            Console.ReadLine();
        }

        #endregion Methods
    }
}