﻿namespace Lightning_Alert.Entities
{
    using Newtonsoft.Json;

    /// <summary>
    /// Class that encapsulates the Asset object
    /// </summary>
    public class Asset
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Asset Name
        /// </summary>
        [JsonProperty("assetName")]
        public string AssetName { get; set; }

        /// <summary>
        /// Gets or sets the Asset Owner
        /// </summary>
        [JsonProperty("assetOwner")]
        public string AssetOwner { get; set; }

        /// <summary>
        /// Gets or sets the QuadKey
        /// </summary>
        [JsonProperty("quadKey")]
        public string QuadKey { get; set; }

        #endregion Properties
    }
}