﻿namespace Lightning_Alert.Entities
{
    /// <summary>
    /// Class that encapsulates the FlashType entity.
    /// </summary>
    public class FlashType
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is a lightning strike or not
        /// </summary>
        public bool LightningStrike { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }
    }
}