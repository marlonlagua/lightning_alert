﻿namespace Lightning_Alert.Entities
{
    using Newtonsoft.Json;

    /// <summary>
    /// Class that encapsulates the Strike entity
    /// </summary>
    public class Strike
    {
        #region Properties

        /// <summary>
        /// Gets or sets the flash type value
        /// </summary>
        [JsonProperty("flashType")]
        public int FlashType { get; set; }

        /// <summary>
        /// Gets or sets the ic height
        /// </summary>
        [JsonProperty("icHeight")]
        public int IcHeight { get; set; }

        /// <summary>
        /// Gets or sets the latitude
        /// </summary>
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude
        /// </summary>
        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets the multiplicity
        /// </summary>
        [JsonProperty("multiplicity")]
        public int Multiplicity { get; set; }

        /// <summary>
        /// Gets or sets the peak amps
        /// </summary>
        [JsonProperty("peakAmps")]
        public int PeakAmps { get; set; }

        /// <summary>
        /// Gets or sets the received time in milliseconds
        /// </summary>
        [JsonProperty("receivedTime")]
        public long ReceivedTime { get; set; }

        /// <summary>
        /// Gets or sets the reserved
        /// </summary>
        [JsonProperty("reserved")]
        public string Reserved { get; set; }

        /// <summary>
        /// Gets or sets the number of sensors
        /// </summary>
        [JsonProperty("numberOfSensors")]
        public int Sensors { get; set; }

        /// <summary>
        /// Gets or sets the strike time in milliseconds
        /// </summary>
        [JsonProperty("strikeTime")]
        public long StrikeTime { get; set; }

        #endregion Properties
    }
}